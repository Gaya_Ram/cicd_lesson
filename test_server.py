#### run on anaconda
##  pytest -v -s --disable-warnings test_server.py::test_initial_data


import pytest
import server
import json

@pytest.fixture
def client(): ##client is an instant of a server
    server.app.config['TESTING'] = True
    client = server.app.test_client()

    yield client

def test_initial_data(client):

    rv = client.get('/tasks')
    data = eval(rv.data)
    print(data)
    assert len(data) == 2

def test_tasks_post(client):

    rv = client.post(
        '/tasks',
        data=json.dumps(dict(title='parcel delivery 0001')),
        content_type='application/json'
    )
    data = eval(rv.data)
    assert data['title'] == 'parcel delivery 0001'
    assert data['id'] == 3
    assert data['completed'] == 'False'


def test_post_data(client):

    rv = client.get('/tasks')
    data = eval(rv.data)
    assert len(data) == 3
